<div class="flex-item">
    <div  id="post-<?php echo get_the_ID()?>" <?php post_class("rt-post rt-post--".rt_option("blog_options_layout", "grid")) ?>>
    
        <?php rt_post_thumbnail() ?>

        <div class="rt-post__body">
            <?php rt_post_category() ?>
            <?php rt_post_title() ?>
            <?php rt_post_meta() ?>
            <?php rt_post_content() ?>
            <?php rt_post_footer() ?>
        </div>

    </div>
</div>