<?php

$account_url = '#';
$trigger_modal_class = '';

if( rt_is_premium()){
  $trigger_modal_class = 'js-modal-login-trigger';
}

if (is_user_logged_in() || rt_is_woocommerce('account_page')) {
   $account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
   $trigger_modal_class = '';
}
 ?>
<a class="rt-header__element header_user <?php echo $trigger_modal_class?>" href="<?php echo esc_url($account_url)?>">
  <i class="rt-header__user-icon ti-user"></i>
</a>
