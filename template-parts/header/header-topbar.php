<?php // Plan - Premium ?>

<?php if (rt_option('header_topbar', false)): ?>

<?php $elements = rt_option_header(); ?>

  <?php do_action('rt_before_header_topbar') ?>
  <div id="header-topbar" class="rt-header__topbar">
      <div class="page-container">

        <div id="header-topbar-left" class="rt-header__column" data-alignment="<?php echo $elements['topbar_left_alignment'] ?>" data-display="<?php echo $elements['topbar_left_display'] ?>">
            <?php do_action('rt_header_topbar_left')?>
        </div>
        
        <div id="header-topbar-center" class="rt-header__column" data-alignment="<?php echo $elements['topbar_center_alignment'] ?>" data-display="<?php echo $elements['topbar_center_display'] ?>">
          <?php do_action('rt_header_topbar_center')?>
        </div>

        <div id="header-topbar-right" class="rt-header__column" data-alignment="<?php echo $elements['topbar_right_alignment'] ?>" data-display="<?php echo $elements['topbar_right_display'] ?>">
            <?php do_action('rt_header_topbar_right')?>
        </div>


      </div>
  </div>
  <?php do_action('rt_after_header_topbar') ?>
  
<?php endif; ?>
