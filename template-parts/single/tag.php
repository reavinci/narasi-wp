<div class="rt-tag mb-40">
  <span class="rt-tag__title">
    <i class="fa fa-tags"></i>
  </span>
  <?php foreach (get_the_tags() as $term): ?>
    <a class="rt-tag__item <?php echo $term->slug?>" href="<?php echo esc_url(get_tag_link($term->term_id))?>">
      <?php echo $term->name ?>
    </a>
  <?php endforeach; ?>
</div>

