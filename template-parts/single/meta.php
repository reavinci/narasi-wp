<?php
$date_url   = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
$avatar     = get_avatar(get_the_author_meta('ID'), 30);
$avatar_url = get_the_author_meta('profile_avatar', get_the_author_meta('ID'));

?>
<div class="rt-single__meta mb-20">

  <?php if (rt_option('single_meta_date', true)): ?>
    <a class="rt-single__meta-item date" href="<?php echo $date_url ?>"><i class="fa fa-calendar"></i><?php echo get_the_date() ?></a>
  <?php endif; ?>

   <?php if (rt_option('single_meta_author', true)): ?>
    <a class="rt-single__meta-item author" href="<?php echo $author_url ?>"><i class="fa fa-user"></i><?php the_author()?></a>
   <?php endif; ?>

   <?php if (rt_option('single_meta_comment', true) && get_comments_number() >= 1 ): ?>
     <span class="rt-single__meta-item comment"><i class="fa fa-comment"></i><?php echo get_comments_number() ?></span>
   <?php endif?>

</div>