<div class="rt-badges">
  <?php foreach (get_the_category() as $term): ?>
    <a class="<?php echo $term->slug?>" href="<?php echo esc_url(get_tag_link($term->term_id))?>">
      <?php echo $term->name ?>
    </a>
  <?php endforeach; ?>
</div>

