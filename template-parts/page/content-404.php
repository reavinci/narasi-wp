<div class="rt-entry-content">
    <h2><?php echo __("Unfortunately, the page you requested could not be found.", 'rt_domain') ?></h2>
    <p><?php echo __("It looks like the link pointing here was faulty. Maybe try searching?", 'rt_domain') ?></p>
    <?php get_template_part('searchform') ?>
</div>