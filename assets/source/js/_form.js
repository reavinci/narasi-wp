jQuery(document).ready(function ($) {
  
    /*=================================================;
    /* FORM CLICK
    /*================================================= */
    var form_overlay = function () {

        var form = $('.js-form-overlay').find('.rt-form__input');

        form.each(function () {
            if ($(this).val() != '') {
                $(this).parents('.rt-form').addClass("is-active");
            }
        });

        form.on('focus', function () {
            form.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.rt-form').removeClass("is-active");
                }
            });

            $(this).parents('.rt-form').addClass("is-active");

        });

    }
    form_overlay();


    // end document
});
