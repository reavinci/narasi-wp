<?php
include_once dirname(__FILE__) . '/class-helper.php';
include_once dirname(__FILE__) . '/class-html.php';
include_once dirname(__FILE__) . '/class-ajax.php';
include_once dirname(__FILE__) . '/class-customizer.php';
include_once dirname(__FILE__) . '/class-notice.php';
include_once dirname(__FILE__) . '/class-activation.php';
require_once dirname(__FILE__) . '/class-templates.php';


add_action('elementor/widgets/widgets_registered', function(){
    include_once dirname(__FILE__) . '/class-elementor.php';
});
