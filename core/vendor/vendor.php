<?php
if (!class_exists('Kirki')) {
    require_once dirname(__FILE__) . '/kirki/kirki.php';
}
require_once dirname(__FILE__) . '/breadcrumb-tail.php';
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';
require_once dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';
require_once dirname(__FILE__) . '/class-gamajo-template-loader.php';
require_once dirname(__FILE__) . '/Mobile_Detect.php';

// merlin
require_once dirname(__FILE__). ('/merlin/vendor/autoload.php' );
require_once dirname(__FILE__). ('/merlin/class-merlin.php' );
require_once dirname(__FILE__). ('/merlin-config.php' );


$theme_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/narasi/', get_template_directory(), 'narasi');
$theme_update->setBranch('stable_release');