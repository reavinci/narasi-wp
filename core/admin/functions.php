<?php
/*=================================================;
/* ADMIN - TEMPLATE PART
/*================================================= */
function rt_admin_get_template_part($template, $data = '')
{
    $loader = new Retheme\Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part("core/admin/views/{$template}");

}

/*=================================================;
/* GET PAGE ADMIN
/*================================================= */
function rt_admin_get_page()
{
    return !empty($_GET['page']) ? $_GET['page'] : '';
}

function rt_admin_theme_page()
{
    return array(
        'theme-dashboard',
        'theme-option',
        'theme-panel',
        'theme-license',
        'theme-pro',
        'theme-contact',
        'tgmpa-install-plugins',
        'pt-one-click-demo-import',
    );
}

/**
 * get admin page url
 *
 * @param [type] $url
 * @return url page
 */
function rt_admin_page_url($url){
    return admin_url()."admin.php?page={$url}";
}


/*=================================================;
/* SHOW NOTIF AFTER THEME ACTIVE
/*================================================= */
function rt_admin_notif_stater(){
    add_action('admin_notices', function(){
        rt_admin_get_template_part('starter');
    });
}
add_action( 'after_switch_theme', 'rt_admin_notif_stater' );