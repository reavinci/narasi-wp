jQuery(document).ready(function ($) {
  $(".retheme-header").parents('li')
                      .addClass('control-header')
                      .attr('data-control', 'control-header');



  var element = $("li.customize-control");

  element.each(function () {
 
    if ($(this).attr('id')) {

    var id = $(this).attr('id');
    var control_id = id.replace('customize-control-', '');
    var control_class = '';

    /*=================================================
    * Responsive Control
    /*================================================= */
    
     if (_wpCustomizeSettings['controls'][control_id] && _wpCustomizeSettings['controls'][control_id]['device']) {
       var control_device = _wpCustomizeSettings['controls'][control_id]['device'];
       var device_icon = ('<ul class="responsive-switchers"><li class="desktop"><span class="preview-desktop" data-device="desktop"><i class="dashicons dashicons-desktop"></i></span></li><li class="tablet"><span class="preview-tablet" data-device="tablet"><i class="dashicons dashicons-tablet"></i></span></li><li class="mobile"><span class="preview-mobile" data-device="mobile"><i class="dashicons dashicons-smartphone"></i></span></li></ul>');

       if (control_device.length != 0 ) {
         $(this).addClass('control-responsive control-device-' + control_device)
                .attr('data-responsive', control_device);
          /**
           * insert device icon each responsive control
           */
          wp.customize.control(control_id, function (control) {
            control.deferred.embedded.done(function () {
              control.container.find('label').append(device_icon);
            });
          });
        
       }

       
       

     }

    
    // end main if
  }
    // each
  });
  
  // end jquery ready
});
