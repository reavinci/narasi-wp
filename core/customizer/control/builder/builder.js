/*=================================================
* CUSTOMIZER
/*================================================= */
!(function($) {
  wp.customize.controlConstructor["builder"] = wp.customize.Control.extend({
    ready: function() {
      var control = this;

      control.init();
    },

    init: function() {
      var control = this;

      var api = wp.customize;
      var setting =
        '<div class="rt-builder-setting js-builder-column-setting"><div class="rt-builder-setting__item js-builder-column-alignment"><h5 class="rt-builder-setting__title">Aligment</h5><ul class="rt-builder-setting__body"><li id="alignment-left" class="is-active"><i class="fa fa-align-left"></i></li><li id="alignment-center"><i class="fa fa-align-center"></i></li><li id="alignment-right"><i class="fa fa-align-right"></i></li><li id="alignment-justify"><i class="fa fa-align-justify"></i></li></ul></div><div class="rt-builder-setting__item js-builder-column-display"><h5 class="rt-builder-setting__title">Display</h5><ul class="rt-builder-setting__body"><li id="display-normal" class="is-active">Normal</li><li id="display-grow">Grow</li></ul></div></div>';

      /**
       * Append Html setting panel each column
       */
      control.container.find(".rt-builder__column").append(setting);

      /**
       * Call Action
       */
      control.show_settings();
      control.alignment();
      control.display();
      control.remove();
      control.focus();
      control.responsive();

      /**
       * Sortable Element
       */
      control.container
        .find(".js-builder-connect")
        .sortable({
          connectWith: control.container.find(".js-builder-connect"),
          dropOnEmpty: true,
          update: function(event, ui) {
            //  var id = ui.item.attr("id");
            // var column = this.id;
            control.update();
          },
          activate: function(event, ui) {}
        })
        .disableSelection();

      /**
       * Builder Tabs
       */
      control.container.find("#tabs").tabs();

      /**
       * Panel Customizer
       * Set header builder full witdh if panel customizer close
       */
      $(this).addClass("is-active");
      var builder = $("#" + control.container.attr("id")).parent();

      builder.addClass("expanded");

      $(".collapse-sidebar").on("click", function() {
        $(this).css("z-index", "99999");
        if ($(this).hasClass("is-active")) {
          $(this).removeClass("is-active");
          builder.addClass("expanded");
        } else {
          $(this).addClass("is-active");
          builder.removeClass("expanded");
        }
      });

      /**
       * Disable Row
       */
      control.disable_row("sticky");
      control.disable_row("middle");
      control.disable_row("topbar");

      /**
       * header builder active
       */

      $("#accordion-panel-header_panel, .js-header-builder-trigger").on(
        "click",
        function() {
          $("#sub-accordion-section-header_builder_section").addClass(
            "is-active"
          );
        }
      );
      $(
        "#sub-accordion-panel-header_panel .customize-panel-back, .js-header-builder-close"
      ).on("click", function() {
        $("#sub-accordion-section-header_builder_section").removeClass(
          "is-active"
        );
      });
    },

    /**
     * Disable row column
     */
    disable_row: function(row) {
      var control = this;
      var newRow = row.replace("header_", "");

      wp.customize("header_" + newRow, function(value) {
        value.bind(function(to) {
          if (to == true) {
            control.container.find("#header-" + newRow).removeClass("disable");

            control.container.find("#header-" + newRow + " .disable").remove();
          } else {
            control.container
              .find("#header-" + newRow)
              .addClass("disable")
              .find(".js-builder-control-focus")
              .append('<span class="disable">- Disable</span>');
          }
        });
      });
    },

    /**
     * Focus control
     */
    focus: function() {
      $(".js-builder-control-focus").on("click", function() {
        // get class control
        var group = $(this).data("class");

        // get id control from class
        var control_id = "retheme_header_" + $(this).data("control");

        // Focus control
        wp.customize.control(control_id).focus();

        // Open control collapse on focus
        // close all control
        if ($("#customize-control-" + control_id).hasClass("is-active")) {
          $("#customize-control-" + control_id).removeClass("is-active");
        } else {
          $(".control-collapse").slideUp();
          $("#customize-control-" + control_id).addClass("is-active");

          $(".control-collapse." + group).slideDown();
        }

        // control active move to top panel
        var position = $("#customize-control-" + control).position();

        $(".wp-full-overlay-sidebar-content").scrollTop(position.top);
      });
    },

    /**
     * Update the sorting element
     */
    update: function() {
      var control = this;

      var newValue = {
        topbar_left_element: control.container
          .find("#header-topbar-left")
          .sortable("toArray"),
        topbar_left_display: control.container
          .find("#header-topbar .left")
          .attr("data-display"),
        topbar_left_alignment: control.container
          .find("#header-topbar .left")
          .attr("data-alignment"),
        topbar_center_element: control.container
          .find("#header-topbar-center")
          .sortable("toArray"),
        topbar_center_display: control.container
          .find("#header-topbar .center")
          .attr("data-display"),
        topbar_center_alignment: control.container
          .find("#header-topbar .center")
          .attr("data-alignment"),
        topbar_right_element: control.container
          .find("#header-topbar-right")
          .sortable("toArray"),
        topbar_right_display: control.container
          .find("#header-topbar .right")
          .attr("data-display"),
        topbar_right_alignment: control.container
          .find("#header-topbar .right")
          .attr("data-alignment"),

        middle_left_element: control.container
          .find("#header-middle-left")
          .sortable("toArray"),
        middle_left_display: control.container
          .find("#header-middle .left")
          .attr("data-display"),
        middle_left_alignment: control.container
          .find("#header-middle .left")
          .attr("data-alignment"),
        middle_center_element: control.container
          .find("#header-middle-center")
          .sortable("toArray"),
        middle_center_display: control.container
          .find("#header-middle .center")
          .attr("data-display"),
        middle_center_alignment: control.container
          .find("#header-middle .center")
          .attr("data-alignment"),
        middle_right_element: control.container
          .find("#header-middle-right")
          .sortable("toArray"),
        middle_right_display: control.container
          .find("#header-middle .right")
          .attr("data-display"),
        middle_right_alignment: control.container
          .find("#header-middle .right")
          .attr("data-alignment"),

        main_left_element: control.container
          .find("#header-main-left")
          .sortable("toArray"),
        main_left_display: control.container
          .find("#header-main .left")
          .attr("data-display"),
        main_left_alignment: control.container
          .find("#header-main .left")
          .attr("data-alignment"),
        main_center_element: control.container
          .find("#header-main-center")
          .sortable("toArray"),
        main_center_display: control.container
          .find("#header-main .center")
          .attr("data-display"),
        main_center_alignment: control.container
          .find("#header-main .center")
          .attr("data-alignment"),
        main_right_element: control.container
          .find("#header-main-right")
          .sortable("toArray"),
        main_right_display: control.container
          .find("#header-main .right")
          .attr("data-display"),
        main_right_alignment: control.container
          .find("#header-main .right")
          .attr("data-alignment"),

        sticky_left_element: control.container
          .find("#header-sticky-left")
          .sortable("toArray"),
        sticky_left_display: control.container
          .find("#header-sticky .left")
          .attr("data-display"),
        sticky_left_alignment: control.container
          .find("#header-sticky .left")
          .attr("data-alignment"),
        sticky_center_element: control.container
          .find("#header-sticky-center")
          .sortable("toArray"),
        sticky_center_display: control.container
          .find("#header-sticky .center")
          .attr("data-display"),
        sticky_center_alignment: control.container
          .find("#header-sticky .center")
          .attr("data-alignment"),
        sticky_right_element: control.container
          .find("#header-sticky-right")
          .sortable("toArray"),
        sticky_right_display: control.container
          .find("#header-sticky .right")
          .attr("data-display"),
        sticky_right_alignment: control.container
          .find("#header-sticky .right")
          .attr("data-alignment"),

        mobile_left_element: control.container
          .find("#header-mobile-left")
          .sortable("toArray"),
        mobile_left_display: control.container
          .find("#header-mobile .left")
          .attr("data-display"),
        mobile_left_alignment: control.container
          .find("#header-mobile .left")
          .attr("data-alignment"),
        mobile_center_element: control.container
          .find("#header-mobile-center")
          .sortable("toArray"),
        mobile_center_display: control.container
          .find("#header-mobile .center")
          .attr("data-display"),
        mobile_center_alignment: control.container
          .find("#header-mobile .center")
          .attr("data-alignment"),
        mobile_right_element: control.container
          .find("#header-mobile-right")
          .sortable("toArray"),
        mobile_right_display: control.container
          .find("#header-mobile .right")
          .attr("data-display"),
        mobile_right_alignment: control.container
          .find("#header-mobile .right")
          .attr("data-alignment"),

        drawer_element: control.container
          .find("#drawer_element")
          .sortable("toArray")
      };

      control.setting.set(newValue);
    },

    /**
     * Show/Hide setting panel each column
     */
    show_settings: function() {
      var control = this;
      var trigger = control.container.find(
        ".js-builder-column-setting-trigger"
      );
      var setting = control.container.find(".js-builder-column-setting");

      trigger.on("click", function() {
        /* show setting box if trigger click */
        if ($(this).hasClass("is-active")) {
          trigger.removeClass("is-active");
          setting.hide();
        } else {
          setting.hide();

          $(this).addClass("is-active");
          $(this)
            .parent()
            .children(".js-builder-column-setting")
            .slideDown()
            .css("display", "flex");
        }
      });
    },

    /**
     * Aligment
     * set aligment column add class flex in header
     */
    alignment: function() {
      var control = this;
      var trigger = control.container.find(".js-builder-column-alignment li");

      /* Default */
      trigger.each(function() {
        var start = $(this)
          .removeClass("is-active")
          .parents(".rt-builder__column")
          .attr("data-alignment");

        $(this)
          .parent()
          .find("#alignment-" + start)
          .addClass("is-active");
      });

      trigger.on("click", function() {
        var id = $(this).attr("id");

        $(this)
          .parent()
          .find("li")
          .removeClass("is-active");

        var alignment = id.replace("alignment-", "");

        $(this)
          .parents(".rt-builder__column")
          .attr("data-alignment", alignment);

        $(this).addClass("is-active");

        /* Update */
        control.update();
      });
    },

    /**
     * Display
     * set display column
     */
    display: function() {
      var control = this;
      var trigger = control.container.find(".js-builder-column-display li");

      /* Default
       * Get aligment attribute for each column
       */
      trigger.each(function() {
        var start = $(this)
          .removeClass("is-active")
          .parents(".rt-builder__column")
          .attr("data-display");

        $(this)
          .parent()
          .find("#display-" + start)
          .addClass("is-active");
      });

      trigger.on("click", function() {
        $(this)
          .parent()
          .find("li")
          .removeClass("is-active");

        var id = $(this).attr("id");

        var display = id.replace("display-", "");

        $(this)
          .parents(".rt-builder__column")
          .attr("data-display", display);

        $(this).addClass("is-active");

        /* Update */
        control.update();
      });
    },

    /**
     * Remove Element
     * move element from drop area to sorce area
     */
    remove: function() {
      var control = this;
      var trigger = control.container.find(".js-builder-element-close");

      trigger.on("click", function() {
        var clone = $(this).parent();
        var tab = $(this)
          .parents(".rt-builder__tab")
          .attr("id");

        clone.detach();

        clone.appendTo("#" + tab + " .js-builder-source");

        /* Update */
        control.update();
      });
    },

    /**
     * Change responsive preview
     */
    responsive: function() {
      var control = this;

      var element = $(".js-preview-devices");
      var all_devices = "preview-desktop preview-tablet preview-mobile";
      var footer_devices = $("#customize-footer-actions .devices");

      element.on("click", function() {
        var device = $(this).data("device");
        $(".wp-full-overlay")
          .removeClass(all_devices)
          .addClass("preview-" + device);

        /**
         * Footer panel device trigger syc with header builder
         */
        footer_devices
          .find("button")
          .removeClass("active")
          .attr("aria-pressed", "false");
        footer_devices
          .find("button.preview-" + device)
          .addClass("active")
          .attr("aria-pressed", "true");
      });
    }

    /* end class */
  });
})(window.jQuery);