<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Page extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_page_settings();
        $this->add_page_title();
        $this->add_page_sub_title();

    }

    public function set_section()
    {
        $this->add_section('', array(
            'page_header' => array(esc_attr__('Page', 'rt_domain')),
        ));
    }

    public function add_page_settings()
    {

        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'page_header',
            'section' => 'page_header_section',
            'class' => 'page_header',
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'page_header',
            'label' => __('Enable Page Header', 'rt_domain'),
            'section' => 'page_header_section',
            'class' => 'page_header',
            'default' => true,
        ));

        if ( rt_is_premium()) {
            $this->add_field(array(
                'type' => 'radio-image',
                'settings' => 'page_header_style',
                'label' => __('Style', 'rt_domain'),
                'section' => 'page_header_section',
                'class' => 'page_header',
                'default' => 'left',
                'choices' => array(
                    'split' => get_template_directory_uri() . '/core/customizer/assets/img/page-header-split.svg',
                    'left' => get_template_directory_uri() . '/core/customizer/assets/img/page-header-left.svg',
                    'right' => get_template_directory_uri() . '/core/customizer/assets/img/page-header-right.svg',
                    'center' => get_template_directory_uri() . '/core/customizer/assets/img/page-header-center.svg',
                ),
            ));

        }

        $this->add_field_background(array(
            'settings' => 'page_header_background',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'element' => '.page-header',
        ));

        $this->add_field(array(
            'type' => 'background',
            'settings' => 'page_header_background',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'label' => __('Background', 'rt_domain'),
            'default' => array(
                'background-color' => '',
                'background-image' => '',
                'background-repeat' => 'repeat',
                'background-position' => 'center center',
                'background-size' => 'cover',
                'background-attachment' => 'scroll',
            ),
            'output' => array(
                array(
                    'element' => '.page-header',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_border_color(array(
            'settings' => 'page_header_border_color',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'element' => '.page-header',
        ));

        if ( rt_is_premium()) {
            $this->add_field_responsive(array(
                'type' => 'dimensions',
                'settings' => 'page_header_padding',
                'label' => __('Spacing', 'rt_domain'),
                'section' => 'page_header_section',
                'class' => 'page_header',
                'default' => [
                    'padding-top' => '45px',
                    'padding-bottom' => '45px',
                ],
                'output' => array(
                    array(
                        'element' => '.page-header',
                    ),
                    array(
                        'element' => '.page-header',
                    ),

                ),
                'transport' => 'auto',
            ));
        }

    }

    public function add_page_title()
    {

        $this->add_header(array(
            'label' => 'Title',
            'settings' => 'page_title',
            'section' => 'page_header_section',
            'class' => 'page_title',
        ));

        if ( rt_is_premium()) {
            $this->add_field_responsive(array(
                'type' => 'typography',
                'settings' => 'page_title_typography',
                'label' => __('Typography', 'rt_domain'),
                'section' => 'page_header_section',
                'class' => 'page_title',
                'default' => array(
                    'variant' => rt_var('font-weight'),
                    'font-size' => '',
                    'line-height' => '',
                    'text-transform' => 'none',
                ),
                'output' => array(
                    array(
                        'element' => '.page-header__title',
                    ),
                ),
                'transport' => 'auto',
            ));

        }

        $this->add_field_color(array(
            'settings' => 'page_title_color',
            'section' => 'page_header_section',
            'class' => 'page_title',
            'element' => '.page-header__title',
        ));

    }

    public function add_page_sub_title()
    {

        $this->add_header(array(
            'label' => 'Description',
            'settings' => 'page_description',
            'section' => 'page_header_section',
            'class' => 'page_description',
        ));

        if ( rt_is_premium()) {
            $this->add_field_responsive(array(
                'type' => 'typography',
                'settings' => 'page_description_typography',
                'label' => __('Typography', 'rt_domain'),
                'section' => 'page_header_section',
                'class' => 'page_description',
                'default' => array(
                    'variant' => '',
                    'font-size' => '',
                    'line-height' => '',
                    'text-transform' => 'none',
                ),
                'output' => array(
                    array(
                        'element' => '.page-header__desc',
                    ),
                ),
                'transport' => 'auto',
            ));

        }

        $this->add_field_color(array(
            'settings' => 'page_description_color',
            'section' => 'page_header_section',
            'class' => 'page_description',
            'element' => '.page-header__desc',
        ));

    }

// end class
}

new Page;
