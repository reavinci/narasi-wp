<?php
/*=================================================;
/* MENU
/*================================================= */
/**
 * Add css class to menu item
 * @category Menu
 * @param [type] $classes
 * @param [type] $item
 * @return void
 */
function rt_menu_class($classes, $item)
{
    if ($item->current) {
        $classes[] = "is-active";
    }

    $classes[] = "rt-menu__item";

    return $classes;

}
add_filter('nav_menu_css_class', 'rt_menu_class', 10, 2);

/**
 * add class sub menu
 * @category menu
 * @param [type] $menu
 * @return void
 */
function rt_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="sub-menu rt-menu__submenu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'rt_submenu_class');

