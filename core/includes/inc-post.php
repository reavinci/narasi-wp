<?php
/*=================================================;
/* GET ARCHIVE ALL POST
/*================================================= */
/**
 * This function detect all archive on post type post
 * like category, tag, archive default
 *
 * @return boolean
 */
function rt_is_post_archive()
{
    if (is_category() || is_tag() || is_archive() || is_home() || is_search()) {
        return true;
    }
}

/*=================================================
 *  POST WRAPPER CLASS
/*================================================= */
/**
 * Added class to before loop class post
 */

function rt_post_loop_class($classes = '')
{
    if (rt_is_post_archive()) {
        $classes[] = 'rt-post-archive flex flex-loop flex-row';
    }

    if (rt_is_post_archive() && rt_option('blog_options_layout', 'grid') == 'grid') {
        $column = 12 / rt_option('blog_options_column', 3);
        $column_md = 12 / rt_option('blog_options_column_tablet', 2);
        $column_sm = 12 / rt_option('blog_options_column_mobile', 2);

        $classes[] = 'flex-cols-lg-' . $column;
        $classes[] = 'flex-cols-md-' . $column_md;
        $classes[] = 'flex-cols-xs-' . $column_sm;

        /** Masonry layout enable */
        if (rt_option('blog_options_masonry', false)) {
            $classes[] = 'js-masonry';
        }

    } else if (rt_is_post_archive() && rt_option('blog_options_layout', 'grid') == 'list') {
        $classes[] = 'flex-cols-lg-12';
    }

    return $classes;

}
add_filter('rt_loop_wrapper_class', 'rt_post_loop_class');

/*=================================================;
/* POST AJAX QUERY
/*================================================= */
function rt_post_query_loop($loop)
{
    if (rt_is_post_archive()) {
        $loop = array(
            'post_type' => 'post',
            'posts_per_page' => rt_option('posts_per_page', 7),
            'template_part' => 'template-parts/post/content-post',
            'pagination' => rt_option('blog_options_pagination', 'number'),
        );

    }
    return $loop;
};

add_filter('rt_loop_query', 'rt_post_query_loop');

/*=================================================
 *  BODY CLASS
/*================================================= */
function rt_body_class($classes)
{
    if (is_singular('post')) {
        $classes[] = 'template--single';
    }
    return $classes;
}
add_filter('body_class', 'rt_body_class');

/*=================================================
 *  POST LOOP
/*================================================= */
function rt_post_title()
{
    if (rt_option('blog_element_title', true)) {
        rt_get_template_part('post/title');
    }
}

function rt_post_thumbnail()
{
    if (rt_option('blog_element_thumbnail', true)) {
        rt_get_template_part('post/thumbnail');
    }
}

function rt_post_category()
{
    if (!empty(get_the_category()) && rt_option('blog_meta_categories', true)) {
        rt_get_template_part('post/category');
    }
}
function rt_post_meta()
{
    rt_get_template_part('post/meta');
}

function rt_post_content()
{
    if (rt_option('blog_element_content', true)) {
        rt_get_template_part('post/content');
    }
}

function rt_post_footer()
{
    if (rt_option('blog_element_readmore', true)) {
        rt_get_template_part('post/footer');
    }
}

/*=================================================
 *  POST SINGLE
/*===================================== */

function rt_single_title()
{
    if (rt_option('single_element_title', true)) {
        rt_get_template_part('single/title');
    }
}

function rt_single_thumbnail()
{
    if (rt_option('single_element_thumbnail', true)) {
        rt_get_template_part('single/thumbnail');
    }
}

function rt_single_meta()
{
    rt_get_template_part('single/meta');
}

function rt_single_content()
{
    if (rt_option('single_element_content', true)) {
        rt_get_template_part('single/content');
    }
}

function rt_single_author_info()
{
    rt_get_template_part('single/author-info');
}

function rt_single_navigation()
{
    if (rt_option('single_element_navigation', true)) {
        rt_get_template_part('single/navigation');
    }
}

function rt_single_related()
{
    if (get_the_category() && rt_option('single_element_related', true)) {
        rt_get_template_part('single/related');
    }
}

function rt_single_comment()
{
    if (comments_open() || get_comments_number() && rt_option('single_element_comment', true)) {
        rt_get_template_part('single/comment');
    }
}

function rt_single_category()
{
    if (!empty(get_the_category()) && rt_option('single_meta_categories', true)) {
        rt_get_template_part('single/category');
    }
}

function rt_single_tags()
{
    if (!empty(get_the_tags()) && rt_option('single_element_tags', true)) {
        rt_get_template_part('single/tags');
    }
}

function rt_single_share()
{
    rt_get_template_part('global/share');
}

/*=================================================;
/* ACTION
/*================================================= 
* remove page header on single template 
*/
function rt_post_remove_page_title($title)
{
    if (is_single()) {
        $title = false;
    }
    return $title;
}
add_filter('rt_page_title', 'rt_post_remove_page_title');


/*=================================================;
/* POST COUNTER AJAX
/*================================================= */
function rt_post_count_scripts()
{
    if(rt_option('post_counter', true)){
        global $wp_query;

        wp_enqueue_script('post_counter', get_template_directory_uri() . '/assets/js/post-counter.min.js', array('jquery'), '1.0.0', true);

        wp_localize_script(
            'post_counter',
            'post_counter',
            array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('rml-nonce'),
                'posts' => json_encode($wp_query->query_vars),

            )
        );
    }
}

add_action('wp_enqueue_scripts', 'rt_post_count_scripts');

function rt_post_count_result()
{   
     if(rt_option('post_counter', true) && !empty($_POST['post_id'])){
        $postID = str_replace("post-counter-", "", $_POST['post_id']);
        $count_key = 'wp_post_views_count';

        if (!is_admin()) {

            $count = get_post_meta($postID, $count_key, true);
            if ($count == '') {
                $count = 0;
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '1');
            } else {
                ++$count;
                update_post_meta($postID, $count_key, $count);
            }
        }
        echo get_post_meta($postID, $count_key, true);

        die();
    }
}

add_action('wp_ajax_ajax_post_count_result', 'rt_post_count_result');
add_action('wp_ajax_nopriv_ajax_post_count_result', 'rt_post_count_result');
