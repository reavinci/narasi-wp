<?php
/*=================================================;
/* ARCHIVE - REMOVE PREFIX PAGE TITLE
/*=================================================
 * remove prefix archive on category
 */
function rt_archive_remove_prefix_title($title)
{
    if (is_category() || is_tag()) {
        $title = single_cat_title('', false);
    }
    return $title;

}
add_filter('get_the_archive_title', 'rt_archive_remove_prefix_title');
