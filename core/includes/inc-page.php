<?php
/*=================================================
 * PAGE - TITLE
=================================================== */
function rt_get_page_title()
{

    $title = get_the_title();

    if (is_archive()) {
        $title = get_the_archive_title();
        $desc = get_the_archive_description();
    }

    if (is_home()) {
        $title = __('Lastest Post', 'rt_domain');
    }

    if (is_404()) {
        $title = __('404', 'rt_domain');
    }

    if (is_search()) {
        $title = __('Search Results for: ', 'rt_domain') . '<span>' . get_search_query() . '</span>';
    }

    if (rt_is_woocommerce('shop')) {
        $title = __('Shop', 'woocommerce');
    }

    return apply_filters('rt_page_title', $title);
}

/*=================================================
 * PAGE - DESC
=================================================== */
function rt_get_page_desc()
{
    $desc = '';
    if (is_archive()) {
        $desc = get_the_archive_description();
    }
    if (rt_is_woocommerce('shop')) {
        $desc = '';
    }
    return apply_filters('rt_page_desc', $desc);
}
